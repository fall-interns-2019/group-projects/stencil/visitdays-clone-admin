import { Config } from '@stencil/core';

// https://stenciljs.com/docs/config

export const config: Config = {
  outputTargets: [{
    type: 'www',
    serviceWorker: null
  },
    { type: 'dist' }
  ],
  namespace: 'visitdays-clone-admin',
  globalScript: 'src/global/app.ts',
  globalStyle: 'src/global/app.css',
  // excludeSrc: ['**/src/components/fake-profile/**','**/src/components/dummy-root/**', '**/src/components/sandbox/**'],

  copy: [
    { src: '../node_modules/@fullmeasure/icons', dest: 'assets/icons' },
    { src: '../node_modules/@fullmeasure/images', dest: 'assets/images' },
  ],
};
