import {Component, Event, EventEmitter, h, Prop, State} from '@stencil/core';
import {RouteService} from "../../services/route.service";
import {Survey as SurveyModel} from "../../models/survey.model";

@Component({
  tag: 'survey-form-details',
  styleUrl: 'survey-form-details.css'
})
export class SurveyFormDetails {
  @Prop({mutable: true}) survey: any;
  @Prop({mutable: true}) errors: any;
  @State() params: any;
  @Event() surveyUpdated: EventEmitter;
  @State() questions: any[] = [];

  async componentWillLoad() {
    this.params = RouteService.params();
    if (this.survey.survey_pages) {
      this.questions = this.survey.survey_pages;
    }
  }

  updateSurvey(event) {
    let newValue = event.target.value;
    const newSurvey = {...new SurveyModel(), ...this.survey};
    newSurvey[event.target.name] = newValue;
    this.survey = {...new SurveyModel(), ...newSurvey};
    this.surveyUpdated.emit({survey: this.survey});
    console.log('called updateSurvey');
  }

  // renderError(field) {
  //   if (this.errors[field] === undefined) {
  //     return;
  //   }
  //   return (
  //     <ion-note style={{paddingLeft: '16px'}} color='danger'>{this.errors[field][0].message}</ion-note>
  //   );
  // }

  addQuestion() {
    const newQuestionArray = [...this.questions];
    newQuestionArray.push({
      title: null,
      page_number: newQuestionArray.length + 1,
      question_json: [],
      survey_id: this.params.id
    });
    this.questions = newQuestionArray;
    this.updateSurvey({target: {name: 'survey_pages', value: this.questions}})
  }

  addOption(question_index) {
    const newQuestionsArray = [...this.questions];
    newQuestionsArray[question_index].question_json.push({
      text: null,
    });
    this.questions = newQuestionsArray;
    this.updateSurvey({target: {name: 'survey_pages', value: this.questions}})
  }

  updateQuestion(event, question_index, option_index?) {
    const newQuestionsArray = [...this.questions];
    if (option_index !== null) {
      newQuestionsArray[question_index].question_json[option_index].text = event.target.value;
    } else {
      newQuestionsArray[question_index].title = event.target.value;
    }
    this.questions = newQuestionsArray;
    this.updateSurvey({target: {name: 'survey_pages', value: this.questions}});
  }

  renderQuestions() {
    return this.questions.map((question, question_index) => {
      if (typeof question.question_json === 'string') {
        question.question_json = JSON.parse(question.question_json)
      }
      return [
        question_index > 0 ?
          <ion-item-divider /> : null,
        <ion-item>
          <ion-label position='floating'>Question {question_index + 1}</ion-label>
          <ion-input
            disabled={this.survey.published}
            required={true}
            type='text'
            placeholder='Type Question'
            value={question.title}
            onIonInput={(event) => this.updateQuestion(event, question_index, null)}/>
        </ion-item>,
        question.question_json.map((option, option_index) => {
          return [
            <ion-item>
              <ion-label position='floating'>Option {option_index + 1}</ion-label>
              <ion-input
                disabled={this.survey.published}
                required={true}
                type='text'
                placeholder='Type Option'
                value={option.text}
                onIonInput={(event) => this.updateQuestion(event, question_index, option_index)}/>
            </ion-item>
          ]
        }),
        <ion-button onClick={() => this.addOption(question_index)}>
          Add Option
        </ion-button>
      ]
    })
  }

  render() {
    return [
      <ion-content class='scroll-padding'>
        <ion-list lines='full'>
          <ion-item>
            <ion-label position='floating'>Survey name</ion-label>
            <ion-input
              disabled={this.survey.published}
              required={true}
              name='name'
              type='text'
              value={this.survey.name}
              onInput={(event) => this.updateSurvey(event)}/>
          </ion-item>
            <ion-button onClick={() => this.addQuestion()}>
              Add Question
            </ion-button>
          {this.renderQuestions()}
        </ion-list>
      </ion-content>
    ];
  }
}
