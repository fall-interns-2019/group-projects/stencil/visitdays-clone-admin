import {Component, h, State, Prop, Listen} from '@stencil/core';
import {RouteService} from '../../services/route.service';
import {SessionService} from '../../services/session.service';

@Component({
  tag: 'event-management',
  styleUrl: 'event-management.css'
})
export class EventManagement {
  params: any = {};
  session: any = {};
  @State() selectedTab = 'Registrants';
  @State() saved = true;
  @State() disabled = false;
  @Prop({ connect: 'ion-alert-controller' }) alertController: HTMLIonAlertControllerElement;
  @Prop({ connect: 'ion-toast-controller' }) toastController: HTMLIonToastControllerElement;


  async componentWillLoad() {
    this.params = RouteService.params();
    this.session = SessionService.get();
  }

  @Listen('ionChange')
  listener(event) {
    if (event.target.tagName !== 'ION-SEGMENT') { return; }
    this.selectedTab = event.detail.value;
  }

  renderTab() {
    if (this.selectedTab === 'Registrants') {
      return this.renderRegistrants();
    } else if (this.selectedTab === 'Details') {
      return this.renderDetails();
    }
  }

  renderDetails() {
    return [
      <events-form />
    ];
  }

  renderRegistrants() {
    return [
      <event-registrants-tab />
    ];
  }


  renderTabs() {
    return (
      <ion-segment>
        <ion-segment-button value='Registrants' color='primary' checked={this.selectedTab === 'Registrants'}>Registrants</ion-segment-button>
        <ion-segment-button value='Details' color='primary'>Details</ion-segment-button>
      </ion-segment>
    );
  }



  render() {
    return [
      <ion-header>
        <ion-toolbar >
          <ion-buttons slot='start'>
            <ion-button
              slot='icon-only'
              href= {`/#/events/?organization_id=${this.params.organization_id}`}
            >
              <ion-icon name='arrow-back'/>
            </ion-button>
            <ion-menu-button></ion-menu-button>
          </ion-buttons>
          <ion-title>Event</ion-title>
          <ion-buttons slot='end' >
          </ion-buttons>
        </ion-toolbar>
        {this.renderTabs()}
      </ion-header>,
      this.renderTab()
    ];

  }
}
