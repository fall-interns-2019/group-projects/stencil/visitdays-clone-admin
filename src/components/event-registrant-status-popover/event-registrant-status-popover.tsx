import {Component, h, Listen, Prop} from '@stencil/core';
import {popoverController} from "@ionic/core";

@Component({
  tag: 'event-registrant-status-popover',
  styleUrl: 'event-registrant-status-popover.css'
})
export class EventRegistrantStatusPopover {
  @Prop() value: number;

  @Listen('ionChange')
  async updateStatus(event) {
    if(event.target.nodeName !== 'ION-RADIO-GROUP') return;
    await popoverController.dismiss(event.detail.value);
  }

  render() {
    return [
      <ion-list>
        <ion-radio-group value={this.value}>
          <ion-item>
            <ion-label>Not Here</ion-label>
            <ion-radio slot='start' value={0}/>
          </ion-item>
          <ion-item>
            <ion-label>Checked In</ion-label>
            <ion-radio slot='start' value={1}/>
          </ion-item>
          <ion-item>
            <ion-label>Cancelled</ion-label>
            <ion-radio slot='start' value={2}/>
          </ion-item>
        </ion-radio-group>
      </ion-list>
    ];
  }
}
