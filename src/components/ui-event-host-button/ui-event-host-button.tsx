import {Component, Event, EventEmitter, Listen, Prop, h} from '@stencil/core';
import {modalController} from "@ionic/core";

@Component({
  tag: 'ui-event-host-button',
})
export class UiEventHostButton {

  @Prop({mutable: true}) model: any;
  @Prop() disabled?: any;
  @Prop() required = false;
  @Prop() foreignKey: string;
  @Event() hostsChanged: EventEmitter;
  modal: any;

  async openSearchModal() {
    this.modal = await modalController.create({
      component: 'ui-event-host-search',
      componentProps: {
        user_events: this.model.user_events
      },
      backdropDismiss: false
    });
    await this.modal.present();
  }

  @Listen('ionModalWillDismiss', {target: 'body'})
  updateHosts(event) {
    const data = event.detail.data;
    console.log('dismissed data:', data);
    if(!data) return;
    this.hostsChanged.emit(data);
  }

  hasRecipients() {
    return (this.model.user_events !== undefined && this.model.user_events.length > 0)
  }

  renderIcons() {
    if (this.hasRecipients()) {
      return [
        <ion-button>
          <ion-badge color='primary' >
            {this.model.user_events.length}
          </ion-badge>
        </ion-button>
      ];
    } else {
      return (
        <ion-button>
          <ion-icon src='assets/icons/add_circle.svg' />
        </ion-button>
      );
    }
  }

  render() {
    if (this.model === undefined) { return; }
    return [
      <ion-item button={true} disabled={this.disabled} onClick={() => this.openSearchModal()}>
        <ion-label>Hosts</ion-label>
        <ion-buttons slot='end' >
          {this.renderIcons()}
        </ion-buttons>
      </ion-item>
    ];
  }
}
