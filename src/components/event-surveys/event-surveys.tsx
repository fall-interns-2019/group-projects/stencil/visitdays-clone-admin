import {Component, h, Listen, State} from '@stencil/core';
import {UUID} from 'angular2-uuid';
import _ from 'underscore';
// import {format, setDay} from 'date-fns';
import {RouteService} from "../../services/route.service";
import {UiSearchService} from "../../services/ui-search.service";
import {SurveyHttpService} from "../../http_services/survey.http_service";

@Component({
  tag: 'event-surveys',
  styleUrl: 'event-surveys.css'
})
export class EventSurveys {
  params: any;
  needle = '';
  filterOptions: { value: string, display: string }[];
  list: HTMLIonListElement;

  @State() surveys: any[];
  @State() fabMessage = 'CREATE YOUR FIRST SURVEY';
  @State() searchTerm = '';
  @State() filteredSurveys: any[];
  @State() filterType = {value: 'all', display: 'All'};

  async componentWillLoad() {
    this.filterOptions = [
      {value: 'all', display: 'All'},
      {value: 'drafts', display: 'Draft'},
      {value: 'published', display: 'Published'},
      {value: 'archived', display: 'Archived'},
    ];
    this.params = RouteService.params();
    const data = await new SurveyHttpService().query({
      organization_id: this.params.organization_id,
    });

    this.filteredSurveys = _.sortBy([...data], (survey: any) => survey.created_at);
    this.surveys = data;
  }

  @Listen('filterSelected', {target: 'body'})
  updateFilter(event) {
    if (event.type === 'filterSelected') {
      this.filterType = event.detail.data;
    } else if (event.type === 'input') {
      this.needle = event.target.value;
    }
    const copyEvents = [...this.surveys];
    let filtered = [];
    if (this.filterType.value === 'all') {
      filtered = copyEvents;
    } else if (this.filterType.value === 'drafts') {
      filtered = _.where(copyEvents, {published: false, archived: false});
    } else if (this.filterType.value === 'published') {
      filtered = _.where(copyEvents, {published: true});
    } else if (this.filterType.value === 'archived') {
      filtered = _.where(copyEvents, {archived: true});
    } else {
      filtered = copyEvents;
    }
    const results = UiSearchService.search(filtered, this.needle, ['name']);
    this.filteredSurveys = _.sortBy([...results], (survey: any) => survey.created_at);
  }

  uuid() {
    return UUID.UUID();
  }

  async archive(survey) {
    const payload = {...survey, archived: !survey.archived};
    const response = await new SurveyHttpService().put(payload);
    _.findWhere(this.surveys, {id: response.id}).archived = true;
    this.updateFilter({});
    await this.list.closeSlidingItems();
  }

  async duplicate(survey) {
    const payload = {...survey, published: false, archived: false, id: UUID.UUID()};
    const response = await new SurveyHttpService().post(payload);
    this.surveys = [...this.surveys, response];
    this.updateFilter({});
    await this.list.closeSlidingItems();
  }


  renderStatusIndicator(survey) {
    if (survey.archived) {
      return (
        <ion-note slot='end' color='danger'>
          Archived
        </ion-note>
      );
    } else if (survey.published) {
      return (
        <ion-icon slot='end' style={{color: '#10dc60'}} src='assets/icons/cloud_circle.svg'/>
      );
    } else {
      return (
        <ion-note slot='end'>
          Draft
        </ion-note>
      );
    }
  }

  @Listen('ionClear')
  search(survey) {
    this.updateFilter(survey);
  }

  renderActionButtons(survey) {
    return [
      <ion-item-options side='start'>
        <ion-item-option
          style={{color: 'white'}}
          color='danger'
          onClick={async () => {
            await this.archive(survey);
          }}>
          <ion-icon slot='icon-only' src='assets/icons/delete.svg'/>
        </ion-item-option>
      </ion-item-options>,
      <ion-item-options side='end'>
        <ion-item-option
          style={{color: 'white'}}
          color='primary'
          onClick={async () => {
            await this.duplicate(survey);
          }}>
          <ion-icon slot='icon-only' name='copy'/>
        </ion-item-option>
      </ion-item-options>
    ];
  }

  renderSurveys() {

    return this.filteredSurveys.map((survey) => {
      return [
        <ion-item-sliding slot='top'>
          {this.renderActionButtons(survey)}
          <ion-item style={{'--padding-start': '0px', '--inner-padding-end': '0px'}}
                    href={`#/events/surveys/form/?organization_id=${survey.organization_id}&id=${survey.id}`}>
            <ion-card style={{width: '100%'}}>
              <ion-card-header>
                <ion-card-title>
                  <ion-item class="ion-no-padding">
                    <b style={{'font-size': '1.3em'}}>{survey.name}</b>
                    {this.renderStatusIndicator(survey)}
                  </ion-item>
                </ion-card-title>
              </ion-card-header>
            </ion-card>
          </ion-item>
        </ion-item-sliding>
      ]
    });
  }

  renderContent() {
    if (this.surveys !== undefined && this.surveys.length === 0) {
      return (
        <ui-empty-state message='You have not created any surveys, yet.'/>
      );
    } else {
      return (
        <ion-list ref={el => this.list = el as HTMLIonListElement} lines='none'>
          <ion-item-divider>
            <ion-label>
              {this.filterType.display.toUpperCase()}
            </ion-label>
          </ion-item-divider>
          {this.renderSurveys()}
        </ion-list>
      );
    }
  }

  render() {
    if (this.filteredSurveys === undefined) {
      return;
    }
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot='start'>
            <ion-button slot='icon-only' href={`#/profile/?organization_id=${this.params.organization_id}`}>
              <ion-icon name='arrow-back'/>
            </ion-button>
            <ion-menu-button></ion-menu-button>
          </ion-buttons>
          <ion-title>Surveys</ion-title>
          <ion-buttons slot='end'>
            <cm-filter-button name='surveyFilter' options={this.filterOptions} selected={this.filterType.value}/>
          </ion-buttons>
        </ion-toolbar>
        <ion-toolbar>
          <ion-searchbar onInput={(event) => this.search(event)}/>
        </ion-toolbar>
      </ion-header>,
      <ion-content>
        {this.renderContent()}
      </ion-content>,
      <ui-new-fab route='events/surveys/form' message={this.surveys.length === 0 ? this.fabMessage : undefined}
                  data-cy='new-survey-button'/>
    ];
  }
}

