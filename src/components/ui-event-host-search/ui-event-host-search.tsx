import {Component, Element, h, Listen, Prop, State} from '@stencil/core';
import _ from 'underscore';
import {AvatarService} from "../../services/avatar.service";
import {RouteService} from "../../services/route.service";
import {SessionService} from "../../services/session.service";
import {UserHttpService} from "../../http_services/user.http_service";
import {UserSearchHttpService} from "../../http_services/user_search.http_service";

@Component({
  tag: 'ui-event-host-search',
})
export class UiEventHostSearch {
  params: any = {};
  session: any = {};
  debounced: any;
  modal: any;
  sendFrom: any;
  avatarService = new AvatarService();
  @Prop() user_events: any[];
  @State() user: any;
  @State() saving = false;
  @State() users: any[] = [];
  @State() selected: any[] = [];
  @State() newMessage: string;
  @Element() el: Element;

  async componentWillLoad() {
    this.params = RouteService.params();
    this.session = SessionService.get();
    this.user = await new UserHttpService().find(this.session.user_id);
    this.sendFrom = {...this.user};
    if(this.user_events.length > 0) {
      this.selected = await Promise.all(this.user_events.map(async (user_event) => {
        return await new UserHttpService().findBy({
          id: user_event.user_id,
          organization_id: this.params.organization_id
        });
      }));
      console.log('selected:', this.selected);
    }
  }

  debounce(event) {
    if (this.debounced === undefined) {
      this.debounced = _.debounce(() => this.search(this, event), 500, false);
    }
    this.debounced(this, event);
  }

  async search(_this, event) {
    _this.users = (await new UserSearchHttpService().query({
      query: event.target.value,
      organization_id: _this.params.organization_id
    })).filter((user) => !this.selected.find((_user) => _user.id === user.id));
  }

  async update(event) {
    this.newMessage = event.srcElement.value;
  }

  send() {
    this.saving = true;
  }

  @Listen('ionChange')
  updateSendFrom(event) {
    if (event.target.nodeName !== 'ION-SELECT') {
      return;
    }
    this.sendFrom = event.detail.value;
  }

  dismiss(payload: any, role: string) {
    if (_.isEmpty(payload)) {
      return this.el.closest('ion-modal').dismiss([], 'simple-dismiss');
    } else {
      this.el.closest('ion-modal').dismiss(payload, role);
    }
  }

  renderUsers(show_selected) {
    const list = show_selected ? 'selected' : 'users';
    return this[list].map((user, index) => {
      return [
        <ion-item lines='none' button={true} onClick={async () => {
          const updated_selected = [...this.selected];
          const updated_users = [...this.users];
          if (show_selected) {
            updated_selected.splice(index, 1);
            // updated_users.push({...user});
          } else {
            updated_users.splice(index, 1);
            updated_selected.push({...user});
          }
          this.selected = updated_selected;
          this.users = updated_users;
          await this.search(this, {
            target: {
              value: this.el.querySelector('ion-searchbar').value
            }
          });
        }}>
          <ion-avatar slot='start'>
            <img src={this.avatarService.picture(user)}/>
          </ion-avatar>
          <ion-label>
            <h3>
              {user.first_name} {user.last_name}
            </h3>
          </ion-label>
        </ion-item>
      ];
    });
  }

  renderUserGroup() {
    if (this.users.length === 0) {
      return;
    }
    return [
      <ion-item-group>
        <ion-item-divider>
          <ion-label>Contacts</ion-label>
        </ion-item-divider>
        {this.renderUsers(false)}
      </ion-item-group>
    ];
  }

  renderSearchResults() {
    return [
      this.selected.length > 0 ? (
        <ion-item-group>
          <ion-item-divider>
            <ion-label>Selected</ion-label>
          </ion-item-divider>
          {this.renderUsers(true)}
        </ion-item-group>
      ) : null,
      this.users.length === 0 ? (
        <ui-empty-state message={'No results matching search.'}/>
      ) : this.renderUserGroup()
    ];
  }


  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot='start'>
            <ion-button
              slot='icon-only'
              onClick={() => this.dismiss(this.selected, 'search-closed')}>
              <ion-icon name='close'/>
            </ion-button>
            <ion-menu-button/>
          </ion-buttons>
          <ion-title>
            Add Event Host
          </ion-title>
        </ion-toolbar>
        <ion-toolbar>
          <ion-searchbar search-icon={'assets/icons/contacts.svg'} onInput={(event) => this.debounce(event)}/>
        </ion-toolbar>
      </ion-header>,
      <ion-content style={{display: 'flex', flexDirection: 'column-reverse'}}>
        {this.renderSearchResults()}
      </ion-content>
    ];
  }
}
