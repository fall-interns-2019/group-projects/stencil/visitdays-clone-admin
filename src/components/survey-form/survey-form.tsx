import {Component, h, Listen, State} from '@stencil/core';
// import _ from 'underscore';
import {Survey} from "../../models/survey.model";
// import {toastController} from '@ionic/core';
import {RouteService} from "../../services/route.service";
import {SessionService} from "../../services/session.service";
import {SurveyHttpService} from '../../http_services/survey.http_service';
import {SurveyPageHttpService} from "../../http_services/survey_page.http_service";

@Component({
  tag: 'survey-form',
  styleUrl: 'survey-form.css'
})
export class SurveyForm {
  @State() params: any;
  @State() survey: any;
  @State() saved = true;
  @State() disabled = false;
  @State() errors: any = {};

  @Listen('surveyUpdated')
  updateDetail(event) {
    this.survey = {...this.survey, ...event.detail.survey};
    this.saved = false;
    // this.validate(this.survey);
  }

  async componentWillLoad() {
    this.params = RouteService.params();
    const surveyData = await new SurveyHttpService().findBy({
      id: this.params.id,
      organization_id: this.params.organization_id
    });
    const userId = SessionService.get().user_id;
    const surveyDefaults = {
      ...new Survey(),
      id: this.params.id,
      organization_id: this.params.organization_id,
      last_modified_by_user_id: userId,
      created_by_user_id: userId,
    };
    if (surveyData && surveyData.survey_pages) {
      surveyData.survey_pages = surveyData.survey_pages.map((survey_page) => {
        survey_page.question_json = JSON.parse(survey_page.question_json);
        return survey_page
      })
    }
    this.survey = {...surveyDefaults, ...surveyData};
  }

  // errorMessages() {
  //   return _.map(this.errors, (error) => {
  //     return error[0].message;
  //   }).join('\n');
  // }
  //
  // async presentCouldNotSaveToast() {
  //   const toast = await toastController.create({
  //     header: 'CANNOT PUBLISH!',
  //     message: this.errorMessages(),
  //     position: 'bottom',
  //     color: 'danger',
  //     duration: 2000,
  //     buttons: [
  //       {
  //         text: 'Ok',
  //         role: 'cancel',
  //         handler: () => {
  //           console.log('Cancel clicked');
  //         }
  //       }
  //     ]
  //   });
  //
  //   return await toast.present();
  // }

  // validate(payload) {
  //   this.errors = {};
  //   if ([undefined, null, ''].includes(payload.name)) {
  //     this.errors['name'] = [{error: 'required', message: 'Must provide a event name.'}];
  //   }
  //   if ([undefined, null, ''].includes(payload.location)) {
  //     this.errors['location'] = [{error: 'required', message: 'Must provide a event location.'}];
  //   }
  //   if ([undefined, null, ''].includes(payload.description)) {
  //     this.errors['description'] = [{error: 'required', message: 'Must provide a event description.'}];
  //   }
  //   if ([undefined, null, ''].includes(payload.max_capacity)) {
  //     this.errors['max_capacity'] = [{error: 'required', message: 'Must provide a event max capacity.'}];
  //   }
  //   if (payload.scheduled) {
  //     if ([undefined, null, ''].includes(payload.start_datetime)) {
  //       this.errors['start_datetime'] = [{error: 'required', message: 'Must provide a start date/time'}];
  //     }
  //     if ([undefined, null, ''].includes(payload.end_datetime)) {
  //       this.errors['end_datetime'] = [{error: 'required', message: 'Must provide an end date/time'}];
  //     }
  //     if (![undefined, null, ''].includes(payload.start_datetime) && ![undefined, null, ''].includes(payload.end_datetime)) {
  //       if (payload.start_datetime >= payload.end_datetime) {
  //         this.errors['end_datetime'] = this.errors['end_datetime'] || [{
  //           error: 'required',
  //           message: 'Start time must be before end time'
  //         }];
  //       }
  //     }
  //     if (payload.repeating) {
  //
  //     }
  //   }
  //   if(payload.user_events.length === 0) {
  //     this.errors['user_events'] = [{error: 'required', message: 'Must provide at least one host.'}];
  //   }
  // }

  @Listen('onSave')
  async completeSave(event) {
    this.disabled = true;
    const published = event.detail.published;
    const archived = event.detail.archived;
    const survey_payload = {...this.survey, published: published, archived: archived};
    // this.validate(survey_payload);
    // if (!_.isEmpty(this.errors) && survey_payload.published) {
    //   this.disabled = false;
    //   await this.presentCouldNotSaveToast();
    //   return;
    // }
    delete survey_payload.survey_pages;
    const survey_response = await new SurveyHttpService().upsert(survey_payload);
    survey_response.survey_pages = await new SurveyPageHttpService().post({
      survey_id: this.params.id,
      survey_pages: this.survey.survey_pages.map((survey_page) => {
        survey_page.question_json = JSON.stringify(survey_page.question_json);
        return survey_page
      }),
      organization_id:  this.params.organization_id
    });
    survey_response.survey_pages = await Promise.all(survey_response.survey_pages.map(async (survey_page) => {
      survey_page.question_json = JSON.parse(survey_page.question_json);
      return survey_page
    }));
    this.saved = true;
    this.survey = {...survey_response};
    this.disabled = false;
    this.saved = true;
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot='start'>
            <ion-button
              slot='icon-only'
              href={`#/events/surveys/?organization_id=${this.params.organization_id}`}
            >
              <ion-icon name='arrow-back'/>
            </ion-button>
          </ion-buttons>
          <ion-buttons slot='end'>
            <ui-save-header-buttons
              archived={this.survey.archived}
              saved={this.saved}
              published={this.survey.published}
              disabled={this.disabled}
              afterArchive={`/events/surveys/?organization_id=${this.params.organization_id}`}
            />
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <survey-form-details survey={this.survey}/>
    ];
  }
}
