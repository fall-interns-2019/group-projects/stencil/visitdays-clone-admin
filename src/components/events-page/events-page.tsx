import {Component, h, Listen, State} from '@stencil/core';
import {UUID} from 'angular2-uuid';
import _ from 'underscore';
import {format, setDay} from 'date-fns';
import {Event} from "../../models/event.model";
import {RouteService} from "../../services/route.service";
import {EventHttpService} from "../../http_services/event.http_service";
import {DateService} from "../../services/date.service";
import {UiSearchService} from "../../services/ui-search.service";

@Component({
  tag: 'events-page',
  styleUrl: 'events-page.css'
})
export class List {
  params: any;
  needle = '';
  filterOptions: {value: string, display: string}[];
  list: HTMLIonListElement;
  days_of_week = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  @State() events: any[];
  @State() fabMessage = 'CREATE YOUR FIRST EVENT';
  @State() searchTerm = '';
  @State() filteredEvents: any[];
  @State() filterType = {value: 'all', display: 'All'};

  async componentWillLoad() {
    this.filterOptions = [
      {value: 'all', display: 'All'},
      {value: 'drafts', display: 'Draft'},
      {value: 'published', display: 'Published'},
      {value: 'archived', display: 'Archived'},
      {value: 'ended', display: 'Ended'},
    ];
    this.params = RouteService.params();
    const data = await new EventHttpService().query({
      organization_id: this.params.organization_id,
    });
    const eventData = [];
    data.forEach((event) => {
      if (!event.repeating) {
        eventData.push(event);
        return;
      }
      const parsed = JSON.parse(event.event_repeat.times_json);
      parsed.forEach((occurrence) => {
        Object.keys(occurrence.days).forEach((day) => {
          if(!occurrence.days[day]) return;
          const today = new Date();
          const day_date = setDay(today, this.days_of_week.indexOf(day));
          const newEvent = {...event};
          const currentStartTime = new Date(newEvent.start_datetime);
          const occurrenceStartTime = new Date(occurrence.start_time);
          const currentEndTime = new Date(newEvent.end_datetime);
          const occurrenceEndTime = new Date(occurrence.end_time);
          currentStartTime.setTime(occurrenceStartTime.getTime());
          currentEndTime.setTime(occurrenceEndTime.getTime());
          currentStartTime.setDate(day_date.getDate());
          currentEndTime.setDate(day_date.getDate());
          newEvent.start_datetime = currentStartTime.toISOString();
          newEvent.end_datetime = currentEndTime.toISOString();
          eventData.push(newEvent);
        });
      });
    });
    this.filteredEvents = _.sortBy(this.unfinishedFilter([...eventData]), (event: any) => event.start_datetime);
    this.events = eventData;
  }

  unfinishedFilter(events: Event[]) {
    const unarchived = _.where(events, {archived: false});
    return unarchived.filter((event: any) => {
      const endDateTimestamp =  DateService.timestamp(event.end_datetime);
      const currentTimestamp = new Date().getTime();
      return !event.scheduled || endDateTimestamp >= currentTimestamp;
    });
  }


  @Listen('filterSelected', {target: 'body'})
  updateFilter(event) {
    if (event.type === 'filterSelected') {
      this.filterType = event.detail.data;
    } else if (event.type === 'input') {
      this.needle = event.target.value;
    }
    const copyEvents = [...this.events];
    let filtered = [];
    if (this.filterType.value === 'all') {
      filtered = this.unfinishedFilter(copyEvents);
    } else if (this.filterType.value === 'drafts') {
      filtered = _.where(copyEvents, {published: false, archived: false});
    } else if (this.filterType.value === 'published') {
      filtered = _.where(this.unfinishedFilter(copyEvents), {published: true});
    } else if (this.filterType.value === 'archived') {
      filtered = _.where(copyEvents, {archived: true});
    } else if (this.filterType.value === 'ended') {
      filtered = _.where(copyEvents, {archived: false, published: true});
      filtered = filtered.filter((event) => {
        const endDateTimestamp =  DateService.timestamp(event.end_datetime);
        const currentTimestamp = new Date().getTime();
        return ![null, undefined].includes(event.end_datetime) && endDateTimestamp <= currentTimestamp;
      });
    } else {
      filtered = copyEvents;
    }
    const results = UiSearchService.search(filtered, this.needle, ['name']);
    this.filteredEvents = _.sortBy([...results], (event: any) => event.start_datetime);
  }

  uuid() {
    return UUID.UUID();
  }

  async archive(event) {
    const payload = {...event, archived: !event.archived };
    const response = await new EventHttpService().put(payload);
    _.findWhere(this.events, {id: response.id}).archived = true;
    this.updateFilter({});
    await this.list.closeSlidingItems();
  }

  async duplicate(event) {
    const payload = {...event, published: false, archived: false, id: UUID.UUID() };
    const response = await new EventHttpService().post(payload);
    this.events = [...this.events, response];
    this.updateFilter({});
    await this.list.closeSlidingItems();
  }


  renderStatusIndicator(event) {
    if (event.archived) {
      return (
        <ion-note slot='end' color='danger'>
          Archived
        </ion-note>
      );
    } else if (event.published) {
      return (
        <ion-icon  slot='end' style={{color: '#10dc60'}} src='assets/icons/cloud_circle.svg' />
      );
    } else {
      return (
        <ion-note slot='end'>
          Draft
        </ion-note>
      );
    }
  }

  @Listen('ionClear')
  search(event) {
    this.updateFilter(event);
  }

  renderActionButtons(event) {
    return [
      <ion-item-options side='start'>
        <ion-item-option
          style={{color: 'white'}}
          color='danger'
          onClick={async () => { await this.archive(event); }}>
          <ion-icon slot='icon-only' src='assets/icons/delete.svg' />
        </ion-item-option>
      </ion-item-options>,
      <ion-item-options  side='end'>
        <ion-item-option
          style={{color: 'white'}}
          color='primary'
          onClick={async () => { await this.duplicate(event); }}>
          <ion-icon slot='icon-only' name='copy' />
        </ion-item-option>
      </ion-item-options>
    ];
  }

  renderEvents() {
    const grouped = _.groupBy(this.filteredEvents, (event) => {
      const today = new Date();
      const start = new Date(event.start_datetime);
      const end = new Date(event.end_datetime);
      if(today >= start && today <= end) {
        if(start.toDateString() === end.toDateString()) {
          return start.toDateString();
        } else {
          return `${start.toDateString()} - ${end.toDateString()}`;
        }
      } else {
        return start.toDateString();
      }
    });
    Object.keys(grouped).forEach((dateStr) => {
      grouped[dateStr] = _.sortBy(grouped[dateStr], 'start_datetime');
    });
    return Object.keys(grouped).map((dateStr) => {
      return [
        <ion-item-divider>
          <ion-label>
            {dateStr === new Date().toDateString() ? 'Today' : dateStr}
          </ion-label>
        </ion-item-divider>,
        grouped[dateStr].map((event) => {
          const start = new Date(event.start_datetime);
          const end = new Date(event.end_datetime);
          return (
            <ion-item-sliding slot='top' >
              {this.renderActionButtons(event)}
              <ion-item style={{'--padding-start': '0px', '--inner-padding-end': '0px'}} href={`#/events/management/?organization_id=${event.organization_id}&id=${event.id}`} >
                <ion-card style={{width: '100%'}} >
                  <ion-card-header>
                    <ion-card-title>
                      <ion-item class="ion-no-padding">
                        <b style={{'font-size': '1.3em'}}>{event.name}</b>
                        {this.renderStatusIndicator(event)}
                      </ion-item>
                    </ion-card-title>
                    <ion-label>
                      {`${format(start, 'p')} - ${format(end, 'p')}`}
                      <br/>
                      {event.location}
                    </ion-label>
                  </ion-card-header>
                  <ion-card-content>
                    {event.description}
                  </ion-card-content>
                </ion-card>
              </ion-item>
            </ion-item-sliding>
          );
        })
      ]
    });
  }

  renderContent() {
    if (this.events !== undefined && this.events.length === 0) {
      return (
        <ui-empty-state message='You have not created any events, yet.' />
      );
    } else {
      return  (
        <ion-list ref={el => this.list = el as HTMLIonListElement}  lines='none'>
          <ion-item-divider>
            <ion-label>
              {this.filterType.display.toUpperCase()}
            </ion-label>
          </ion-item-divider>
          {this.renderEvents()}
        </ion-list>
      );
    }
  }

  render() {
    if (this.filteredEvents === undefined) { return; }

    return [
      <ion-header>
        <ion-toolbar >
          <ion-buttons slot='start'>
            <ion-button slot='icon-only' href={`#/profile/?organization_id=${this.params.organization_id}`}>
              <ion-icon name='arrow-back'/>
            </ion-button>
            <ion-menu-button></ion-menu-button>
          </ion-buttons>
          <ion-title>Events</ion-title>
          <ion-buttons slot='end'>
            <cm-filter-button name='eventFilter' options={this.filterOptions} selected={this.filterType.value} />
          </ion-buttons>
        </ion-toolbar>
        <ion-toolbar>
          <ion-searchbar onInput={ (event) => this.search(event) } />
        </ion-toolbar>
      </ion-header>,
      <ion-content>
        {this.renderContent()}
      </ion-content>,
      <ui-new-fab route='events/form' message={this.events.length === 0 ? this.fabMessage : undefined} data-cy='new-event-button'/>
    ];
  }
}
