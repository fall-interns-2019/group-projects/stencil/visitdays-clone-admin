import {Component, h, Listen, State} from '@stencil/core';
import _ from 'underscore';
import {Event} from "../../models/event.model";
import {toastController} from '@ionic/core';
import {RouteService} from "../../services/route.service";
import {EventHttpService} from "../../http_services/event.http_service";
import {SessionService} from "../../services/session.service";
import {EventRepeat} from "../../models/event_repeat.model";
import {EventRepeatHttpService} from "../../http_services/event_repeat.http_service";
import {UserEventHttpService} from "../../http_services/user_event.http_service";

@Component({
  tag: 'events-form',
  styleUrl: 'events-form.css'
})
export class Page {
  @State() params: any;
  @State() event: any;
  @State() repeat: EventRepeat;
  @State() saved = true;
  @State() disabled = false;
  @State() errors: any = {};

  @Listen('eventUpdated')
  updateDetail(event) {
    this.event = {...this.event, ...event.detail.event};
    this.saved = false;
    this.validate(this.event);
    console.log('eventUpdated');
  }

  @Listen('repeatUpdated')
  updateRepeat(event) {
    this.repeat = {...this.repeat, ...event.detail._repeat};
    this.saved = false;
    console.log('repeatUpdated');
  }

  async componentWillLoad() {
    this.params = RouteService.params();
    const eventData = await new EventHttpService().findBy({
      id: this.params.id,
      organization_id: this.params.organization_id
    });
    console.log('data:', eventData);
    const userId = SessionService.get().user_id;
    const eventDefaults = {
      ...new Event(),
      id: this.params.id,
      organization_id: this.params.organization_id,
      last_modified_by_user_id: userId,
      created_by_user_id: userId,
    };
    const repeatData = eventData ? eventData.event_repeat : null;
    const repeatDefaults = {
      ...new EventRepeat(),
      event_id: this.params.id,
      organization_id: this.params.organization_id,
      last_modified_by_user_id: userId,
      created_by_user_id: userId,
    };
    if (repeatData && repeatData.times_json) {
      repeatData.times_json = JSON.parse(repeatData.times_json);
    }
    this.event = {...eventDefaults, ...eventData};
    this.repeat = {...repeatDefaults, ...repeatData};
  }

  errorMessages() {
    return _.map(this.errors, (error) => {
      return error[0].message;
    }).join('\n');
  }

  async presentCouldNotSaveToast() {
    const toast = await toastController.create({
      header: 'CANNOT PUBLISH!',
      message: this.errorMessages(),
      position: 'bottom',
      color: 'danger',
      duration: 2000,
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });

    return await toast.present();
  }

  validate(payload) {
    this.errors = {};
    if ([undefined, null, ''].includes(payload.name)) {
      this.errors['name'] = [{error: 'required', message: 'Must provide a event name.'}];
    }
    if ([undefined, null, ''].includes(payload.location)) {
      this.errors['location'] = [{error: 'required', message: 'Must provide a event location.'}];
    }
    if ([undefined, null, ''].includes(payload.description)) {
      this.errors['description'] = [{error: 'required', message: 'Must provide a event description.'}];
    }
    if ([undefined, null, ''].includes(payload.max_capacity)) {
      this.errors['max_capacity'] = [{error: 'required', message: 'Must provide a event max capacity.'}];
    }
    if (payload.scheduled) {
      if ([undefined, null, ''].includes(payload.start_datetime)) {
        this.errors['start_datetime'] = [{error: 'required', message: 'Must provide a start date/time'}];
      }
      if ([undefined, null, ''].includes(payload.end_datetime)) {
        this.errors['end_datetime'] = [{error: 'required', message: 'Must provide an end date/time'}];
      }
      if (![undefined, null, ''].includes(payload.start_datetime) && ![undefined, null, ''].includes(payload.end_datetime)) {
        if (payload.start_datetime >= payload.end_datetime) {
          this.errors['end_datetime'] = this.errors['end_datetime'] || [{
            error: 'required',
            message: 'Start time must be before end time'
          }];
        }
      }
      if (payload.repeating) {

      }
    }
    if(payload.user_events.length === 0) {
      this.errors['user_events'] = [{error: 'required', message: 'Must provide at least one host.'}];
    }
  }

  @Listen('onSave')
  async completeSave(event) {
    this.disabled = true;
    const published = event.detail.published;
    const archived = event.detail.archived;
    const event_payload = {...this.event, published: published, archived: archived};
    this.validate(event_payload);
    if (!_.isEmpty(this.errors) && event_payload.published) {
      this.disabled = false;
      await this.presentCouldNotSaveToast();
      return;
    }
    const user_events = [...event_payload.user_events];
    delete event_payload.user_events;
    delete event_payload.event_registrants;
    delete event_payload.event_repeat;
    const event_response = await new EventHttpService().upsert(event_payload);
    event_response.user_events = await new UserEventHttpService().post({
      event_id: this.params.id,
      user_events,
      organization_id: this.params.organization_id
    });
    let repeat_payload, repeat_response;
    if (this.event.repeating) {
      repeat_payload = {...this.repeat};
      repeat_payload.times_json = JSON.stringify(repeat_payload.times_json);
      repeat_response = await new EventRepeatHttpService().upsert(repeat_payload);
      repeat_response.times_json = JSON.parse(repeat_response.times_json);
    }
    this.saved = true;
    this.event = {...event_response};
    if (this.event.repeating) {
      this.repeat = {...repeat_response};
    }
    this.disabled = false;
    this.saved = true;
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          {RouteService.path().includes('form') ? [
            <ion-buttons slot='start'>
              <ion-button
                slot='icon-only'
                href={`#/events/?organization_id=${this.params.organization_id}`}
              >
                <ion-icon name='arrow-back'/>
              </ion-button>
              <ion-menu-button/>
            </ion-buttons>,
            <ion-title>Event</ion-title>
          ] : null}
          <ion-buttons slot='end'>
            <ui-save-header-buttons
              archived={this.event.archived}
              saved={this.saved}
              published={this.event.published}
              disabled={this.disabled}
              afterArchive={`/events/?organization_id=${this.params.organization_id}`}
            />
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <events-form-details event={this.event} repeat={this.repeat} errors={this.errors}/>
    ];
  }
}
