import {Component, Event, EventEmitter, h, Prop, State} from '@stencil/core';
import {AvatarService} from "../../services/avatar.service";
import {pluralize} from "../../helpers/utils";
import {Constants} from "../../config/constants";
import {popoverController} from "@ionic/core";

@Component({
  tag: 'event-registrant-item',
  styleUrl: 'event-registrant-item.css'
})
export class EventRegistrantItem {
  avatarService = new AvatarService();
  @Prop({mutable: true}) event_registrant: any;
  @Prop() registrant: any;
  @State() status: number;
  @Event() registrantUpdated: EventEmitter;

  componentWillLoad() {
    this.status = this.event_registrant.status;
  }

  async updateStatus(newStatus) {
    const newEventRegistrant = {...this.event_registrant};
    newEventRegistrant.status = newStatus;
    delete newEventRegistrant.registrant;
    this.event_registrant = newEventRegistrant;
    this.status = this.event_registrant.status;
    this.registrantUpdated.emit({event_registrant: this.event_registrant});
  }

  async openStatusSelector(event) {
    const popover = await popoverController.create({
      component: 'event-registrant-status-popover',
      componentProps: {
        value: this.status
      },
      event,
      translucent: true,
      showBackdrop: false
    });
    await popover.present();
    const {data} = await popover.onWillDismiss();
    if(data === undefined) return;
    this.updateStatus(data);
  }

  renderStatusSelector() {
    return [
      <ion-button size='default' fill='clear' onClick={(evt) => this.openStatusSelector(evt)}>
        {this.status === Constants.REGISTRATION_STATUS.registered ? null :
          <ion-icon slot='start' name={this.status === Constants.REGISTRATION_STATUS.present
            ? 'checkmark' : 'close-circle'
          }/>
        }
        {
          this.status === Constants.REGISTRATION_STATUS.registered ? 'Not Here'
          : this.status === Constants.REGISTRATION_STATUS.present ? 'Checked In'
          : this.status === Constants.REGISTRATION_STATUS.cancelled ? 'Cancelled'
          : 'Error'
        }
      </ion-button>
    ]
  }

  render() {
    return [
      <ion-item lines="none">
        <ion-avatar slot='start'>
          <img src={this.avatarService.picture(this.registrant)} alt='registrant avatar'/>
        </ion-avatar>
        <ion-label>
          <p>
            <label class="bold-text">
              {`${this.registrant.first_name} ${this.registrant.last_name}`}
            </label>
            <br/>
            {this.event_registrant.guest_count > 0 ?
              `${this.event_registrant.guest_count} 
              ${pluralize('Guest', this.event_registrant.guest_count)}`
              : null}
          </p>
        </ion-label>
        {this.renderStatusSelector()}
        <ion-button size="default" fill="clear">
          <ion-icon slot="icon-only" name="more"/>
        </ion-button>
      </ion-item>
    ];
  }
}
