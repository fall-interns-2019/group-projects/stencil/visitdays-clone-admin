import {Component, h, State} from '@stencil/core';
import {RouteService} from '../../services/route.service';
import {SessionService} from '../../services/session.service';
import {UserHttpService} from '../../http_services/user.http_service';
import {AvatarService} from '../../services/avatar.service';
import {PlatformService} from "../../services/platform.service";

@Component({
  tag: 'profile-page',
  styleUrl: 'profile.css'
})
export class Profile {
  params: any = {};
  session: any = {};
  @State() user: any;
  avatarService = new AvatarService();
  avatar: string;
  platform = PlatformService;

  async componentWillLoad() {
    this.params = RouteService.params();
    this.session = SessionService.get();
    this.user = await new UserHttpService().find(
      this.session.user_id
    );
    this.avatarService = new AvatarService();
    this.avatar = this.avatarService.picture(this.user);
  }

  renderHeader() {
    if (this.user === undefined) { return; }
    if (this.platform.mobile()) {
      return [
        <users-mobile-profile user={this.user} avatar={this.avatarService.picture(this.user)} />
      ];
    } else {
      return [
        <users-desktop-profile user={this.user} avatar={this.avatarService.picture(this.user)} />
      ];
    }
  }

  render() {
    return [
      <ion-content>
        {this.renderHeader()}
        <ion-list>
          <ion-item-divider>
            <ion-label>
              User
            </ion-label>
          </ion-item-divider>
          <auth-logout />
        </ion-list>
      </ion-content>
    ];
  }
}
