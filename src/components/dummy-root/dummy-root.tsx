import {Component, h} from '@stencil/core';
import {RouteConfig} from '../../config/route.config';
import {NavigationService} from '../../services/navigation.service';
import {RouteService} from '../../services/route.service';

@Component({
  tag: 'dummy-root',
})
export class DummyRoot {
  params: any;
  async componentWillLoad() {
    this.params = RouteService.params();
  }

  render() {
    return [
      <ion-app>
        <navigation-root
          routeConfig={RouteConfig.all()}
          navigationService={NavigationService}
          hideWhen={['sign_in', '', 'signup']}/>
      </ion-app>
    ];
  }
}
