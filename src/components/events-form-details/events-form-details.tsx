import {Component, Event, EventEmitter, h, Listen, Prop, State, Watch} from '@stencil/core';
import {RouteService} from "../../services/route.service";
import {convertLocalDateTimeToServer, convertServerDateTimeToLocal} from '../../services/datetime.service';
import {Event as EventModel} from "../../models/event.model";
import {EventRepeat} from "../../models/event_repeat.model";

@Component({
  tag: 'events-form-details',
  styleUrl: 'events-form-details.css'
})
export class Details {
  @Prop({mutable: true}) event: any;
  @Prop({mutable: true}) repeat: any;
  @Prop({mutable: true}) errors: any;
  @State() params: any;
  @State() occurrences: any[] = [];
  @Event() eventUpdated: EventEmitter;
  @Event() repeatUpdated: EventEmitter;

  async componentWillLoad() {
    this.params = RouteService.params();
    if (this.repeat.times_json) {
      this.occurrences = [...this.repeat.times_json];
    }
  }

  @Watch('occurrences')
  handler(newValue, _oldValue) {
    if(!_oldValue || _oldValue.length === 0) return;
    const newRepeat = {...new EventRepeat(), ...this.repeat};
    newRepeat.times_json = newValue;
    this.repeat = newRepeat;
    this.repeatUpdated.emit({_repeat: this.repeat});
    console.log('occurrences was changed!');
  }

  @Listen('hostsChanged')
  updateHosts(event) {
    const newEvent = {...new EventModel(), ...this.event};
    const hosts = event.detail;
    newEvent.user_events = [];
    hosts.forEach((user) => {
      newEvent.user_events.push({
        event_id: this.params.id,
        user_id: user.id
      });
    });
    this.event = {...new EventModel(), ...newEvent};
    this.eventUpdated.emit({event: this.event});
  }

  updateEvent(event) {
    let newValue = event.target.value;
    const newEvent = {...new EventModel(), ...this.event};
    if (event.target.nodeName === 'ION-TOGGLE') {
      newValue = event.detail.checked;
      if (event.target.name === 'scheduled') {
        if (newValue) {
          newEvent.start_datetime = convertServerDateTimeToLocal();
          newEvent.end_datetime = convertServerDateTimeToLocal();
        } else {
          newEvent.start_datetime = null;
          newEvent.end_datetime = null;
        }
      }
    } else if (event.target.nodeName === 'ION-DATETIME') {
      newValue = convertLocalDateTimeToServer(event.detail.value);
    }
    newEvent[event.target.name] = newValue;
    this.event = {...new EventModel(), ...newEvent};
    this.eventUpdated.emit({event: this.event});
    console.log('called updateEvent');
  }

  addOccurrence() {
    this.occurrences = [...this.occurrences, {
      id: Math.random(),
      start_time: null,
      end_time: null,
      days: {}
    }];
  }

  renderError(field) {
    if (this.errors[field] === undefined) {
      return;
    }
    return (
      <ion-note style={{paddingLeft: '16px'}} color='danger'>{this.errors[field][0].message}</ion-note>
    );
  }

  renderDayButtons(occurrence) {
    return (
      ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'].map((day, dayIndex) => {
        return (
          <ion-button disabled={this.event.published}
                      class={dayIndex === 0 ? 'ion-margin-start' : null}
                      color={occurrence.days[day] ? 'primary' : 'light'}
                      onClick={() => {
                        const updated = [...this.occurrences];
                        const index = updated.findIndex((info) => info.id === occurrence.id);
                        updated[index].days[day] = !updated[index].days[day];
                        this.occurrences = updated;
                      }}>
            {day.substr(0, 1)}
          </ion-button>
        )
      })
    )
  }

  renderRepeating() {
    if (!this.event.repeating) {
      return;
    }
    return [
      this.occurrences.map((occurrence, index) => {
        const local_start = occurrence.start_time ? convertServerDateTimeToLocal(occurrence.start_time) : null;
        const local_end = occurrence.end_time ? convertServerDateTimeToLocal(occurrence.end_time) : null;
        return (
          <ion-item>
            <ion-datetime
              disabled={this.event.published}
              name='occurrence'
              min='2013'
              max='2050'
              ion-required={true}
              placeholder={!local_start ? 'Begin' : null}
              value={local_start}
              display-format='h:mm A'
              picker-format='h:mm A'
              onIonChange={(evt) => {
                const updated = [...this.occurrences];
                updated[index].start_time = convertLocalDateTimeToServer(evt.detail.value);
                this.occurrences = updated;
              }}/>
            <ion-datetime
              disabled={this.event.published}
              name='occurrence'
              min='2013'
              max='2050'
              ion-required={true}
              placeholder={!local_end ? 'End' : null}
              value={local_end}
              display-format='h:mm A'
              picker-format='h:mm A'
              onIonChange={(evt) => {
                const updated = [...this.occurrences];
                updated[index].end_time = convertLocalDateTimeToServer(evt.detail.value);
                this.occurrences = updated;
              }}/>
            {this.renderDayButtons(occurrence)}
            {index !== null ? (
              <ion-button disabled={this.event.published} slot="end"
                          fill="clear" size="default"
                          onClick={() => {
                            const updated = [...this.occurrences];
                            updated.splice(index, 1);
                            this.occurrences = updated;
                          }}>
                <ion-icon slot="icon-only" name="trash"/>
              </ion-button>
            ) : null}
          </ion-item>
        )
      }),
      <ion-button disabled={this.event.published}
                  class="ion-padding-horizontal" color="primary"
                  onClick={() => this.addOccurrence()}>
        Add
      </ion-button>
    ];
  }

  renderSchedule() {
    if (!this.event.scheduled) {
      return;
    }
    return [
      <ion-item>
        <ion-label position='floating'>Start Date/Time</ion-label>
        <ion-datetime
          disabled={this.event.published}
          min='2013'
          max='2050'
          ion-required={true}
          name='start_datetime'
          value={this.event.start_datetime}
          display-format='MMM/DD/YYYY @ h:mm A'
          picker-format='MMM/DD/YYYY @ h:mm A'
          onIonChange={(evt) => this.updateEvent(evt)}/>
      </ion-item>,
      <div>{this.renderError('start_datetime')}</div>,
      <ion-item>
        <ion-label position='floating'>End Date/Time</ion-label>
        <ion-datetime
          disabled={this.event.published}
          min='2013'
          max='2050'
          ion-required={true}
          name='end_datetime'
          value={this.event.end_datetime}
          display-format='MMM/DD/YYYY @ h:mm A'
          picker-format='MMM/DD/YYYY @ h:mm A'
          onIonChange={(evt) => this.updateEvent(evt)}/>
      </ion-item>,
      <div>{this.renderError('end_datetime')}</div>,
      <ion-item>
        <ion-label>Repeating</ion-label>
        <ion-toggle
          disabled={this.event.published}
          checked={this.event.repeating} name='repeating' slot='end'
          onIonChange={(evt) => this.updateEvent(evt)}/>
      </ion-item>,
      this.renderRepeating(),
    ];
  }

  render() {
    return [
      <ion-content class='scroll-padding'>
        <ion-list lines='full'>
          <ion-item>
            <ion-label position='floating'>Event name</ion-label>
            <ion-input
              disabled={this.event.published}
              required={true}
              name='name'
              type='text'
              value={this.event.name}
              onInput={(event) => this.updateEvent(event)}/>
          </ion-item>
          {this.renderError('name')}
          <ion-item>
            <ion-label position='floating'>Location</ion-label>
            <ion-input
              disabled={this.event.published}
              required={true}
              name='location'
              type='text'
              value={this.event.location}
              onInput={(event) => this.updateEvent(event)}/>
          </ion-item>
          {this.renderError('location')}
          <ion-item>
            <ion-label position='floating'>Description</ion-label>
            <ion-input
              disabled={this.event.published}
              required={true}
              name='description'
              type='text'
              value={this.event.description}
              onInput={(event) => this.updateEvent(event)}/>
          </ion-item>
          {this.renderError('description')}
          <ion-item>
            <ion-label position='floating'>Max Capacity</ion-label>
            <ion-input
              disabled={this.event.published}
              required={true}
              name='max_capacity'
              type='number'
              min='1'
              value={this.event.max_capacity}
              onInput={(event) => this.updateEvent(event)}/>
          </ion-item>
          {this.renderError('max_capacity')}
          <ui-event-host-button
            disabled={this.event.published}
            required={false}
            model={this.event}
          />
          {this.renderError('user_events')}
          <ion-item-divider>
            <ion-label>
              Display
            </ion-label>
          </ion-item-divider>
          <ion-item>
            <ion-label>Schedule</ion-label>
            <ion-toggle
              disabled={this.event.published}
              checked={this.event.scheduled} name='scheduled' slot='end'
              onIonChange={(evt) => this.updateEvent(evt)}/>
          </ion-item>
          {this.renderSchedule()}
        </ion-list>
      </ion-content>
    ];
  }
}
