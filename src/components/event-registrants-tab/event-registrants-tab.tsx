import {Component, h, Listen, State} from '@stencil/core';
import {RouteService} from '../../services/route.service';
import {SessionService} from '../../services/session.service';
import {EventHttpService} from "../../http_services/event.http_service";
import {Event} from "../../models/event.model";
import {format} from "date-fns";
import _ from 'underscore';
import {AvatarService} from "../../services/avatar.service";
import {UserHttpService} from "../../http_services/user.http_service";
import {Constants} from "../../config/constants";
import {EventRegistrantHttpService} from "../../http_services/event_registrant.http_service";
import {UiSearchService} from "../../services/ui-search.service";

@Component({
  tag: 'event-registrants-tab',
  styleUrl: 'event-registrants-tab.css'
})
export class EventRegistrantsTab {
  params: any = {};
  session: any = {};
  avatarService = new AvatarService();
  needle = '';
  filterOptions: { value: string, display: string }[];

  @State() event: Event | any;
  @State() event_hosts: any[] = [];
  @State() totals = {
    guests: 0,
    registered: 0,
    present: 0,
    cancelled: 0
  };
  @State() filterType: any;
  @State() filtered_event_registrants: any[] = [];

  async componentWillLoad() {
    this.params = RouteService.params();
    this.session = SessionService.get();
    this.filterOptions = [
      {value: 'all', display: 'Registered'},
      {value: '0', display: 'Not Here'},
      {value: '1', display: 'Checked In'},
      {value: '2', display: 'Cancelled'},
    ];
    this.filterType = this.filterOptions[0];
    this.event = await new EventHttpService().findBy({
      id: this.params.id,
      organization_id: this.params.organization_id
    });
    this.event_hosts = await Promise.all(this.event.user_events.map(async (user_event) => {
      return await new UserHttpService().findBy({
        id: user_event.user_id,
        organization_id: this.params.organization_id
      });
    }));
    this.updateTotals();
    this.updateFilter();
    console.log('event:', this.event);
  }

  @Listen('registrantUpdated')
  async handleRegistrantUpdated(event) {
    const data = event.detail;
    if (data.event_registrant) {
      const newEvent = {...this.event};
      const event_registrants = newEvent.event_registrants;
      const index = event_registrants.findIndex((event_registrant) => {
        return event_registrant.id === data.event_registrant.id
      });
      event_registrants[index] = await new EventRegistrantHttpService().put(data.event_registrant);
      this.event = newEvent;
      this.updateTotals();
      this.updateFilter();
    }
  }

  @Listen('filterSelected', {target: 'body'})
  updateFilter(event?) {
    if(!event) {
      event = {
        type: 'filterSelected',
        detail: { data: this.filterType }
      }
    }
    if (event.type === 'filterSelected') {
      this.filterType = event.detail.data;
    } else if (event.type === 'input') {
      this.needle = event.target.value;
    }
    const copyEventRegistrants = [...this.event.event_registrants];
    let filtered = [];
    const numValue = parseInt(this.filterType.value);
    if (!isNaN(numValue)) {
      filtered = _.where(copyEventRegistrants, {status: numValue});
    } else if(this.filterType.value === 'all') {
      filtered = copyEventRegistrants.filter((event_reg) => {
        return [Constants.REGISTRATION_STATUS.registered, Constants.REGISTRATION_STATUS.present]
          .includes(event_reg.status);
      });
    } else {
      filtered = copyEventRegistrants;
    }
    const results = UiSearchService.search(filtered, this.needle, ['first_name', 'last_name']);
    this.filtered_event_registrants = _.sortBy([...results], (event_reg: any) => event_reg.first_name);
  }

  @Listen('ionClear')
  search(event) {
    this.updateFilter(event);
  }

  updateTotals() {
    Object.keys(this.totals).forEach((key) => {
      this.totals[key] = 0;
    });
    this.event.event_registrants.forEach((event_registrant) => {
      this.totals.guests += event_registrant.status !== Constants.REGISTRATION_STATUS.cancelled
        ? event_registrant.guest_count
        : 0;
      this.totals.registered += event_registrant.status === Constants.REGISTRATION_STATUS.registered ? 1 : 0;
      this.totals.present += event_registrant.status === Constants.REGISTRATION_STATUS.present ? 1 : 0;
      this.totals.cancelled += event_registrant.status === Constants.REGISTRATION_STATUS.cancelled ? 1 : 0;
    });
  }

  renderDate() {
    const start = new Date(this.event.start_datetime);
    const end = new Date(this.event.end_datetime);
    return [
      <p>
        {
          start.toDateString() === end.toDateString() ? [
            format(start, 'PP'),
            <br/>,
            `${format(start, 'p')} - ${format(end, 'p')}`
          ] : [
            `${format(start, 'PP, p')} - ${format(end, 'PP, p')}`
          ]
        }
      </p>
    ];
  }

  renderHostAvatars() {
    const more_than_two = this.event_hosts.length > 2;
    return this.event_hosts.map((event_host) => {
      return (
        <ion-avatar slot='start' class={more_than_two ? 'ion-no-margin' : ''}>
          <img style={{
            width: more_than_two ? '50%' : '100%',
            height: more_than_two ? '50%' : '100%'
          }} src={this.avatarService.picture(event_host)} alt='Event host avatar'/>
        </ion-avatar>
      )
    }); 
  }

  renderCheckedInCount() {
    const percent = this.event.event_registrants.length === 0 ? 0 :
      Math.ceil(this.totals.present / this.event.event_registrants.length * 100);
    return (
      <p>
        {`${this.totals.present} (${percent}%) Checked In`}
      </p>
    )
  }

  renderRegistrants() {
    return this.filtered_event_registrants.map((event_registrant) => {
      return (
        <event-registrant-item event_registrant={event_registrant}
                               registrant={event_registrant.registrant}/>
      )
    });
  }

  render() {
    return [
      <ion-content>
        <ion-card>
          <ion-item lines="none">
            <ion-label slot="start">{this.event.name}</ion-label>
            <ion-label class="ion-text-right">
              {this.renderDate()}
            </ion-label>
          </ion-item>
          <ion-item lines="none">
            {this.renderHostAvatars()}
            <ion-label slot="end">
              <b class="ion-form-text">{this.totals.registered + this.totals.present}</b>
              <br/>
              <p>Prosp. Students</p>
            </ion-label>
            <ion-label slot="end">
              <b class="ion-form-text">{this.totals.guests}</b>
              <br/>
              <p>Guests</p>
            </ion-label>
            <ion-label slot="end">
              <b class="ion-form-text">{this.totals.registered + this.totals.present + this.totals.guests}</b>
              <br/>
              <p>Total Registrants</p>
            </ion-label>
            <ion-label slot="end">
              <b class="ion-form-text">
                {
                  `${Math.ceil((this.totals.registered + this.totals.present + this.totals.guests)
                    / this.event.max_capacity * 100)}%`
                }
              </b>
              <br/>
              <p>Capacity Filled</p>
            </ion-label>
          </ion-item>
          <ion-item lines="none" style={{'--background': 'rgba(var(--ion-color-primary-rgb), 0.1)'}}>
            <ion-label slot="start">
              {this.renderCheckedInCount()}
            </ion-label>
          </ion-item>
          <ion-progress-bar value={
            this.event.event_registrants.length === 0 ? 0 : (
              this.totals.present / this.event.event_registrants.length
            )
          }/>
        </ion-card>
        <ion-card>
          <ion-item lines="none" class='ion-no-padding'>
            <ion-searchbar onInput={ (event) => this.search(event) } />
            <ion-buttons>
              <cm-filter-button name='registrantFilter' options={this.filterOptions} selected={this.filterType.value}/>
            </ion-buttons>
          </ion-item>
          <ion-list lines="none">
            {this.renderRegistrants()}
          </ion-list>
        </ion-card>
      </ion-content>
    ];
  }
}
