export class NavigationService {
  static get(params: any): {route: string, label: string, iconName?: string, iconSrc?: string, badge?: {color: string, number: number}}[] {
    if (['undefined', undefined, null, ''].includes(params.organization_id)) {
      // params.organization_id = '52e3d6cc-9ac0-991d-a395-d85c021fccff';
    }
    return [
      {
        route: `/#/profile/?organization_id=${params.organization_id}`,
        iconName: 'contact',
        label: 'Profile',
      },{
        route: `/#/events/?organization_id=${params.organization_id}`,
        iconName: 'list',
        label: 'Events',
      },{
        route: `/#/events/surveys/?organization_id=${params.organization_id}`,
        iconName: 'paper',
        label: 'Surveys',
      }
    ];
  }
}
