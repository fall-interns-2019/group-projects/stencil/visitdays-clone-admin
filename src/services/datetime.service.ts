function pad(num) {
  let norm = Math.floor(Math.abs(num));
  return (norm < 10 ? '0' : '') + norm;
}

function convertToLocalISODateTime(dateTime: string): string {
  const date = new Date(dateTime);
  return date.getFullYear() +
    '-' + pad(date.getMonth() + 1) +
    '-' + pad(date.getDate()) +
    'T' + pad(date.getHours()) +
    ':' + pad(date.getMinutes()) +
    ':' + pad(date.getSeconds()) +
    timezoneOffset();
}

export function convertLocalDateTimeToServer(localDateTime: string): string {
  return new Date(localDateTime).toISOString()
}

export function convertServerDateTimeToLocal(utcDateTime?: string): string {
  if (utcDateTime === undefined || utcDateTime == null) {
    return convertToLocalISODateTime(new Date().toString());
  } else if (utcDateTime.endsWith("Z")) {
    return convertToLocalISODateTime(utcDateTime);
  }
}

export function timezoneOffset(): string {
  let tzo = -new Date().getTimezoneOffset(),
    dif = tzo >= 0 ? '+' : '-';
  return dif + pad(tzo / 60) +
    ':' + pad(tzo % 60);
}
