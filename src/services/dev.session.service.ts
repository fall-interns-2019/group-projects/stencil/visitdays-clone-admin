export class DevSessionService {
  static get(): {token: string, user_id: string} {
    const cached = JSON.parse(localStorage.getItem('dev_session')) || {};
    return {
      token: cached.token,
      user_id: cached.user_id
    };
  }

  static set(data) {
    localStorage.setItem('dev_session', JSON.stringify(data));
  }
}
