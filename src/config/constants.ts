export class Constants {
  public static REGISTRATION_STATUS = {
    registered: 0,
    present: 1,
    cancelled: 2
  }
}
