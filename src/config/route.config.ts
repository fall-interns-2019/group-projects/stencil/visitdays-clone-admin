export class RouteConfig {
  static all() {
    return [
      ...RouteConfig.errors(),
      ...RouteConfig.tabs(),
      ...RouteConfig.adminPanel(),
    ];
  }


  static adminPanel() {
    return [
      {path: 'events', component: 'events-page'},
      {path: 'events/form', component: 'events-form'},
      {path: 'events/management', component: 'event-management'},
      {path: 'events/surveys', component: 'event-surveys'},
      {path: 'events/surveys/form', component: 'survey-form'}
    ]
  }

  static tabs() {
    return [
      {path: '', component: 'auth-login'},
      {path: 'sign_in', component: 'auth-login'},
      {path: 'signup', component: 'auth-create-account'},
      {path: 'profile', component: 'profile-page'},
    ]
  }

  static errors() {
    return [
      {path: 'errors/server', component: 'errors-server'},
      {path: 'errors/unauthorized', component: 'errors-unauthorized'},
      {path: 'errors/not-found', component: 'errors-not-found'},
    ]
  }
}
