import {CrudHttpService} from "./crud.http_service";

export class SurveyPageHttpService extends CrudHttpService {
  constructor() {
    super('events/survey_pages');
    this.url = 'http://localhost:3000/survey_pages';
  }
}
