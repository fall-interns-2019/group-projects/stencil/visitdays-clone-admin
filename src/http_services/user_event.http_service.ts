import {CrudHttpService} from "./crud.http_service";

export class UserEventHttpService extends CrudHttpService {
  constructor() {
    super('events/user_events');
    this.url = 'http://localhost:3000/user_events';
  }
}
