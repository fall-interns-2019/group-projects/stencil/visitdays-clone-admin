import {CrudHttpService} from "./crud.http_service";

export class EventRegistrantHttpService extends CrudHttpService {
  constructor() {
    super('events/event_registrants');
    this.url = 'http://localhost:3000/event_registrants';
  }
}
