import {CrudHttpService} from "./crud.http_service";

export class UserProviderHttpService extends CrudHttpService {
  constructor() {
    super('users/user_providers')
  }
}
