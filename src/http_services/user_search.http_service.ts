import {CrudHttpService} from "./crud.http_service";

export class UserSearchHttpService extends CrudHttpService {
  constructor() {
    super('users/searches');
  }
}
