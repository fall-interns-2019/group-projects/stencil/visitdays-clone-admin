import {CrudHttpService} from "./crud.http_service";

export class EventHttpService extends CrudHttpService {
  constructor() {
    super('events/events');
    this.url = 'http://localhost:3000/events';
  }
}
