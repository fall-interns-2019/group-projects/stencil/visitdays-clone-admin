import {EnvironmentService} from '../services/environment.service';
import {RouteService} from '../services/route.service';
import {SessionStorageService} from '../services/session-storage.service';
import {SessionService} from '../services/session.service';

export class CrudHttpService {
  url: string;
  token: string;
  constructor(apiUrl: string) {
    const env: any = EnvironmentService.config();
    this.url = `${env.schema}://${env.host}/api/${apiUrl}`;
   /* if (apiUrl.includes('auto-responses')) {
      this.url = `http://localhost:3000/${apiUrl.replace('auto-responses/', '')}`;
    }*/
  }

  headers(additional: any = {}): any {
    const token = SessionService.get().token;
    const required = {Authorization: `Token token=${token}`, 'Session-Id': token};
    return {...required, ...additional};
  }

  async query(params: any, cache: boolean = false) {
    const queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
    const fullUrl = `${this.url}?${queryString}`;
    return await this.getRequest(fullUrl, cache);
  }

  async findBy(params: any, cache: boolean = false) {
    const id = params.id;
    const queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
    const fullUrl = `${this.url}/${id}?${queryString}`;
    return await this.getRequest(fullUrl, cache);
  }

  async find_or_create_by(params: any, cache: boolean = false) {
    return await this.findBy(params, cache)
      || await this.post(params);
  }

  async upsert(payload: any) {
    return await this.put(payload)
      || await this.post(payload);
  }

  async put(payload: any) {
    if (payload.id === null || payload.id === undefined) { return; }
    const fullUrl = `${this.url}/${payload.id}`;
    const response = await fetch(fullUrl, {
      method: 'PUT',
      headers: {...this.headers(), 'Content-Type': 'application/json'},
      mode: 'cors',
      redirect: 'follow', // manual, *follow, error
      body: JSON.stringify(payload),

    });
    return await response.json();
  }

  async delete(params: any) {
    if (params.id === null || params.id === undefined) { return; }
    const queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');

    const fullUrl = `${this.url}/${params.id}?${queryString}`;
    const response = await fetch(fullUrl, {
      method: 'DELETE',
      headers: this.headers(),
      mode: 'cors',
    });
    return await response;
  }

  async post(payload: any, params?: any) {
    return await this.postRequest(payload, params);
  }

  async find_by(params?: any) {
    return await this.new('find_by', params);
  }

  async new(action: string, params?: any) {
    params.action = action;
    const queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
    const fullUrl = `${this.url}/new/?${queryString}`;
    return this.getRequest(fullUrl, false, {'X-Organization-Id': params.organization_id});
  }

  async withAssociation(params?: any) {
    return this.new('with_association', params);
  }

  async whereCount(params?: any) {
    return this.new('where_count', params);
  }

  async postRequest(payload: any, params?: any, subRoute?: string) {
    let fullUrl = `${this.url}`;
    if (subRoute !== undefined) {
      fullUrl = `${fullUrl}/new`;
    }
    if (params !== undefined) {
      const queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
      fullUrl = `${fullUrl}?${queryString}`;
    }
    try {
      const response = await fetch(fullUrl, {
        method: 'POST',
        redirect: 'follow', // manual, *follow, error

        headers: {...this.headers(), 'Content-Type': 'application/json'},
        mode: 'cors',
        body: JSON.stringify(payload),
      });
      if ([403, 401].includes(response.status)) {
        if (location.hash.includes('errors')) { return; }
        location.hash = `/errors/unauthorized/?organization_id=${RouteService.params().organization_id}`;
        throw new Error('Unauthorized');
      } else if (response.status === 500) {
        if (location.hash.includes('errors')) { return; }
        location.hash = `/errors/server/?organization_id=${RouteService.params().organization_id}`;
        throw new Error('Internal Server Error');
      } else if (response.status === 503) {
        if (location.hash.includes('errors')) { return; }
        location.hash = `/errors/server/?organization_id=${RouteService.params().organization_id}`;
        throw new Error('Build in progress');
      } else {
        const data = await response.json();
        return data;
      }
    } catch (error) {
      console.error(error);
      if (location.hash.includes('errors')) { return; }
    }
  }

  async find(id: string, cache: boolean = false) {
    const fullUrl = `${this.url}/${id}`;
    return await this.getRequest(fullUrl, cache);
  }

  async getRequest(fullUrl, cache: boolean = false, additionalHeaders: any = {}) {
    const cachedData = SessionStorageService.get(fullUrl);
    if (cachedData && cache) {
      return cachedData;
    }
    try {
      const response = await fetch(fullUrl, {
        method: 'GET',
        headers: this.headers(additionalHeaders),
        mode: 'cors',
      });

      if (response.status === 204) {
        return [];
      } else if ([403, 401].includes(response.status)) {

        if (location.hash.includes('errors')) { return; }
        location.hash = `/errors/unauthorized/?organization_id=${RouteService.params().organization_id}`;
      } else if (response.status === 500) {
        if (location.hash.includes('errors')) { return; }
        location.hash = `/errors/server/?organization_id=${RouteService.params().organization_id}`;
        throw new Error('Internal Server Error');
      } else if (response.status === 503) {
        if (location.hash.includes('errors')) { return; }
        location.hash = `/errors/server/?organization_id=${RouteService.params().organization_id}`;
        throw new Error('Build in progress');
      } else {
        const data = await response.json();
        if (cache) {
          SessionStorageService.set(fullUrl, data);
        }
        return data;
      }
    } catch (error) {
      console.error(error);
      if (location.hash.includes('errors')) { return; }
      location.hash = `/errors/server/?organization_id=${RouteService.params().organization_id}`;
    }
  }

}
