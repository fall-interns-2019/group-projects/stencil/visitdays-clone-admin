import {CrudHttpService} from "./crud.http_service";

export class EventRepeatHttpService extends CrudHttpService {
  constructor() {
    super('events/event_repeats');
    this.url = 'http://localhost:3000/event_repeats';
  }
}
