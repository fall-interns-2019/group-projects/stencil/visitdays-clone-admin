import {CrudHttpService} from "./crud.http_service";

export class SurveyHttpService extends CrudHttpService {
  constructor() {
    super('events/surveys');
    this.url = 'http://localhost:3000/surveys';
  }
}
