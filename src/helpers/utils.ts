export function getCSSValue(property) {
  return getComputedStyle(document.body)
    .getPropertyValue(property)
    .replace('#', '')
    .trim();
}

export function pluralize(word: string, count: number) {
  return count === 1 ? word : (word + 's')
}
