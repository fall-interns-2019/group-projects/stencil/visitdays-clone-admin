import {CrudModel} from './crud.model';

export class Event extends CrudModel {
  public static className = 'Event';
  public organization_id: string;
  public last_modified_by_user_id: string;
  public created_by_user_id: string;
  public name: string;
  public start_datetime: Date;
  public end_datetime: Date;
  public location: string;
  public description: string;
  public max_capacity: number;
  public event_repeat: any;
  public event_registrants: any[] = [];
  public user_events: any[] = [];
  public archived = false;
  public published = false;
  public scheduled = false;
  public repeating = false;
}
