import {CrudModel} from './crud.model';

export class SurveyPage extends CrudModel {
  public static className = 'SurveyPage';
  public organization_id: string;
  public items_json: string;
  public survey_id: string;
  public page_number: string;
}
