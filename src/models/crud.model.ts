export abstract class CrudModel {
  public static className: string;

  public id: string = null;
  public created_at: Date;
  public updated_at: Date;


  public attributes(): any {
    const copy: any = {};
    Object.assign(copy, this);
    return copy;
  }
}
