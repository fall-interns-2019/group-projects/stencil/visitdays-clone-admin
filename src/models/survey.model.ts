import {CrudModel} from './crud.model';

export class Survey extends CrudModel {
  public static className = 'Survey';
  public organization_id: string;
  public last_modified_by_user_id: string;
  public created_by_user_id: string;
  public name: string;
  public archived = false;
  public published = false;
}
